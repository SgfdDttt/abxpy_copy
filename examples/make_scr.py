epochs=256
for layers in [1, 2, 4]:
  for hidden in [128, 256, 512, 1024]:
    out = open("gold_bounds_l"+str(layers)+ "_h"+str(hidden) + "_e"+str(epochs)+"/gold_bounds_l"+str(layers)+ "_h"+str(hidden) + "_e"+str(epochs)+".sh", 'w')
    script=("#!/bin/bash"

    "#- passing some parameters\n"
    "#$ -S /bin/bash        # the shell used\n"
    "#$ -N abx_eval_gold_bounds_l" + str(layers) + "_h" + str(hidden) + "  # gives the name of the job\n"
    "#$ -pe openmpi_ib 8   # nb of cores required (this is purely declarative)\n"
    "#$ -j yes   # join stderr and stdout\n"
    "#$ -q cpu # name of the queue (cpu, gpu, all)\n"
    "#$ -cwd     # puts the logs in the current directory\n"
    "#- some messages to the user\n"
    "/bin/echo Running on host: `hostname`.\n"
    "/bin/echo In directory: `pwd`\n"
    "echo QUEUE=$QUEUE\n"
    "echo NSLOTS=$NSLOTS\n"
    "/bin/echo Starting on: `date`\n"
    "echo load module and env\n"
    "module load python-anaconda\n"
    "source activate abx\n"
    "echo cd /fhgfs/bootphon/scratch/nholzenb/abxpy_copy/examples\n"
    "cd /fhgfs/bootphon/scratch/nholzenb/abxpy_copy/examples\n"
    "echo mkdir -p tmp\n" 
    "mkdir -p tmp\n"
    "echo python complete_run.py gold_bounds_l"+str(layers)+ "_h"+str(hidden) + "_e"+str(epochs)+"/\n"
    "python complete_run.py gold_bounds_l"+str(layers)+ "_h"+str(hidden) + "_e"+str(epochs)+"/\n"
    "# more messages to the user\n"
    "/bin/echo Finished at: `date`")

    out.write(script)


"""

#!/bin/bash

#- passing some parameters
#$ -S /bin/bash        # the shell used 
#$ -N abxscat  # gives the name of the job
#$ -pe openmpi_ib 8   # nb of cores required (this is purely declarative)
#$ -j yes   # join stderr and stdout
#$ -q all.q # name of the queue (cpu, gpu, all)
#$ -cwd     # puts the logs in the current directory

#- some messages to the user
/bin/echo Running on host: `hostname`.
/bin/echo In directory: `pwd`
echo QUEUE=$QUEUE
echo NSLOTS=$NSLOTS
/bin/echo Starting on: `date`

echo load module and env
module load python-anaconda
source activate abx

echo cd /home/nholzenb/abxpy_copy/examples
cd /home/nholzenb/abxpy_copy/examples

echo python complete_run.py buckeye/
python complete_run.py buckeye/

# more messages to the user
/bin/echo Finished at: `date`

"""

