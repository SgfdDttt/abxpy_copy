import numpy as np
import sys
import os
import filecmp
import subprocess
# import h5py
# import numpy as np
# from ys.mods import load
try:
    import h5features
except ImportError:
    sys.path.insert(0, os.path.join(
        os.path.dirname(os.path.dirname(os.path.dirname(
            os.path.realpath(__file__)))), 'h5features'))
    import h5features

files = set()
first_line=True
for line in open(sys.argv[1], 'r'): # file list
    if not first_line:
        files.add(line[:-1].split(' ')[0])
    else:
        first_line=False

files = list(files) 
files2features = dict() # mfccs for each file
files2times = dict() # times for each file
for name in files:
    vec = list()
    for line_mfcc in open(sys.argv[2] + name + '.mfcc'): # mfcc file
        things=line_mfcc[:-1].split(' ')
        if len(things)>1:
            feat = [float(c) for c in things]
            vec.append(feat)
    files2features[name] = vec
    files2times[name] = [0.005 + 0.00125 + 0.01*i for i in range(len(vec))]

features = []
times = []
for filename in files:
    features.append(np.array(files2features[filename]))
    times.append(np.array(files2times[filename]))

if os.path.exists('data.features'):
    os.remove('data.features')

print('files')
print(len(files))
print(files[0])
print('features')
print(len(features))
print(features[0])
print('times')
print(len(times))
print(times[0])

h5features.write('data.features', 'features', files, times, features)

"""
def generate_features(n_files, n_feat=2, max_frames=3, name='data.features'):
    if os.path.exists(name):
        os.remove(name)
    features = []
    times = []
    files = []
    for i in xrange(n_files):
        n_frames = np.random.randint(max_frames) + 1
        features.append(np.random.randn(n_frames, n_feat))
        times.append(np.linspace(0, 1, n_frames))
        files.append('s%d' % i)
    h5features.write(name, 'features', files, times, features)
"""
