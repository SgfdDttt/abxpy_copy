import sys
import math
import cPickle as pickle

files2boundaries=dict()
first_line=True
for line in open(sys.argv[1], 'r'):
    if not first_line:
        things=line[:-1].split(' ')
        filename=things[0]
        if filename not in files2boundaries:
            files2boundaries[filename]=list()
        files2boundaries[filename].append( [ int(round(float(things[1])*100,0))+1, int(round(float(things[2])*100,0))+1 ] )
    else:
        first_line=False

print(files2boundaries)
pickle.dump(files2boundaries, open('buckeye_devpart1_gold_boundaries.pkl', 'wb'))
