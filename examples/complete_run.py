"""This test contains a full run of the ABX pipeline with randomly created
database and features
"""
# -*- coding: utf-8 -*-

import os
import sys
package_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
if not(package_path in sys.path):
    sys.path.append(package_path)
import ABXpy.task
import ABXpy.distances.distances as distances
import ABXpy.distances.metrics.cosine as cosine
import ABXpy.distances.metrics.dtw as dtw
import ABXpy.score as score
import ABXpy.misc.items as items
import ABXpy.analyze as analyze

datadir=sys.argv[1] # contains data.item and data.features
if datadir[-1] == '/':
    datadir=datadir[:-1]

def dtw_cosine_distance(x, y):
    return cosine.cosine_distance(x,y)

def fullrun():
    if not os.path.exists(datadir):
        os.makedirs(datadir)
    item_file = datadir + '/data.item'
    feature_file = datadir + '/data.features'
    distance_file = datadir + '/data.distance'
    scorefilename = datadir + '/data.score'
    taskfilename = datadir + '/data.abx'
    analyzefilename = datadir + '/data.csv'

    # deleting pre-existing files
    for f in [distance_file, scorefilename, taskfilename, analyzefilename]:
        try:
            os.remove(f)
        except OSError:
            pass

    # running the evaluation:
    #items.generate_db_and_feat(3, 3, 1, item_file, 2, 2, feature_file)
    print >> sys.stderr,'START'
    fline=open(item_file).readline().rstrip()
    print >> sys.stderr,'MAKE TASK'
    #categories=fline.split('#')[-1].split(' ')
    #print(categories)
    task = ABXpy.task.Task(item_file, 'word', across='speaker', by=None)
    #task = ABXpy.task.Task(item_file, categories[-3], across=categories[-2], by=categories[-1])
    task.print_stats()
    sys.stdout.flush()
    print >> sys.stderr,'GENERATE TRIPLETS'
    task.generate_triplets(taskfilename, threshold=5, tmpdir='tmp')
    print >> sys.stderr,'COMPUTE DISTANCES'
    distances.compute_distances(feature_file, '/features/', taskfilename,
                                distance_file, dtw_cosine_distance, n_cpu=1)

    print >> sys.stderr,'COMPUTE SCORES'
    score.score(taskfilename, distance_file, scorefilename)
    print >> sys.stderr,'ANALYZE'
    analyze.analyze(taskfilename, scorefilename, analyzefilename)
    print >> sys.stderr,'ALL DONE'
   

fullrun()
