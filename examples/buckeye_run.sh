#!/bin/bash
  
#- passing some parameters
#$ -S /bin/bash        # the shell used 
#$ -N abxscat  # gives the name of the job
#$ -pe openmpi_ib 8   # nb of cores required (this is purely declarative)
#$ -j yes   # join stderr and stdout
#$ -q all.q # name of the queue (cpu, gpu, all)
#$ -cwd     # puts the logs in the current directory
  
#- some messages to the user
/bin/echo Running on host: `hostname`.
/bin/echo In directory: `pwd`
echo QUEUE=$QUEUE
echo NSLOTS=$NSLOTS
/bin/echo Starting on: `date`

echo load module and env
module load python-anaconda
source activate abx

echo cd /home/nholzenb/abxpy_copy/examples
cd /home/nholzenb/abxpy_copy/examples

echo python complete_run.py buckeye/
python complete_run.py buckeye/

# more messages to the user
/bin/echo Finished at: `date`
