import sys
import glob

def item_list(file_name): # return triplets of start time, stop time, word
    relevant=False
    start_times = list()
    words = list()
    for line in open(file_name, 'r'):
        if relevant:
            line = line[:-1].strip(' ')
            start_times.append(line.split(' ')[0])
            words.append(line.split(';')[1].strip(' ').replace(' ', '-'))
        if line[:-1] == '#':
            relevant=True
    out = list() # each item is the info for a line in the item file
    # last word is nothing
    for i in range(1,len(words)):
        if words[i].lower() == words[i]:
            out.append([start_times[i-1], start_times[i], words[i]])
    return out

word_counts = dict()
for thing in glob.glob(sys.argv[1] + '*/*/*.words'):
    filename = thing.split('/')[-1][:-6] # remove .words extension
    items = item_list(thing)
    for item in items:
        if item[2] not in word_counts:
            word_counts[ item[2] ] = 0
        word_counts[ item[2] ] += 1

for word in word_counts: # get rid of hapaxes and reduce count to a max
    if word_counts[word] < 2:
        word_counts[word] = 0
    if word_counts[word] > 20:
        word_counts[word] = 20

print('#file onset offset #speaker word')
for thing in glob.glob(sys.argv[1] + '*/*/*.words'):
    filename = thing.split('/')[-1][:-6] # remove .words extension
    speaker = filename[1:3]
    items = item_list(thing)
    for item in items:
        start_time = float(item[0])
        stop_time = float(item[1])
        if (word_counts[ item[2] ] > 0) and (stop_time-start_time <= 1.5) and (stop_time-start_time >= 0.3):
            print(' '.join([filename, item[0], item[1], speaker, item[2]]))
            word_counts[ item[2] ] -= 1
