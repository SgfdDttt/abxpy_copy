import math
import numpy as np
import sys
import os
import filecmp
import subprocess
# import h5py
# import numpy as np
# from ys.mods import load
try:
    import h5features
except ImportError:
    sys.path.insert(0, os.path.join(
        os.path.dirname(os.path.dirname(os.path.dirname(
            os.path.realpath(__file__)))), 'h5features'))
    import h5features

num_samples=10

print('read file list')
files = set()
first_line=True
for line in open(sys.argv[1], 'r'): # file list
    if not first_line:
        files.add(line[:-1].split(' ')[0])
    else:
        first_line=False

print('load mfcc')
files = list(files) 
files2features = dict() # mfccs for each file
for name in files:
    vec = list()
    for line_mfcc in open(sys.argv[2] + name + '.mfcc'): # mfcc file
        things=line_mfcc[:-1].split(' ')
        if len(things)>1:
            feat = [float(c) for c in things]
            vec.append(feat)
    files2features[name] = vec

print('compute features')
first_line=True
files2embeddings=dict()
files2timestamps=dict()
for line in open(sys.argv[1], 'r'): # file list
    if not first_line:
        things=line[:-1].split(' ')
        filename=things[0]

        start_i=int(float(things[1])*100)
        stop_i=int(float(things[2])*100)
        delta=stop_i-start_i 
        indices=[start_i + int(round(x/float(num_samples-1)*delta,0)) for x in range(num_samples)]
        embed=list()
        for i in indices:
            embed.extend(files2features[filename][i])
        if filename not in files2embeddings:
            files2embeddings[filename]=list()
            files2timestamps[filename]=list()
        files2embeddings[filename].append(embed) 
        files2timestamps[filename].append((float(things[1])+float(things[2]))/2)
    else:
        first_line=False

print('write features to file')
features = []
times = []
for filename in files:
    features.append(np.array(files2embeddings[filename]))
    times.append(np.array(files2timestamps[filename]))

if os.path.exists('data.features'):
    os.remove('data.features')

print('files')
print(len(files))
print(files[0])
print('features')
print(len(features))
print(features[0])
print(len(features[0]))
print(len(features[0][0]))
print('times')
print(len(times))
print(times[0])

h5features.write('data.features', 'features', files, times, features)
