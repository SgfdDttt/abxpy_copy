#!/bin/bash
  
#- passing some parameters
#$ -S /bin/bash        # the shell used 
#$ -N abxscat  # gives the name of the job
#$ -pe openmpi_ib 8   # nb of cores required (this is purely declarative)
#$ -j yes   # ??
#$ -q all.q   # name of the queue (cpu, gpu, all)
#$ -cwd     # puts the logs in the current directory
  
#- some messages to the user
/bin/echo Running on host: `hostname`.
/bin/echo In directory: `pwd`
echo QUEUE=$QUEUE
echo NSLOTS=$NSLOTS
/bin/echo Starting on: `date`

. /etc/profile.d/modules.sh

#which python
module load octave
module load python-anaconda
export PYTHONPATH=$PYTHONPATH:/home/cbergmann/ABXpy
#which python

runname=AIC_acrossSpeaker
directory=/home/cbergmann

#python ABXpy/ABXpy/task.py ${directory}/AIC.item ${directory}/${runname}.abx -o "elem1" -b "elem2" -a "speaker" 

python features_extraction/features.py /fhgfs/bootphon/data/derived_data/AI_LSCP/data/speech/*.wav -h5 ${directory}/${runname}.h5f -c features_extraction/rasta_config.json

#python ABXpy/ABXpy/distance.py ${directory}/${runname}.h5f ${directory}/${runname}.abx ${directory}/${runname}.distance -j 8

#python ABXpy/ABXpy/score.py ${directory}/${runname}.abx ${directory}/${runname}.distance ${directory}/${runname}.score

#python ABXpy/ABXpy/analyze.py ${directory}/${runname}.score ${directory}/${runname}.abx ${directory}/${runname}.csv


# more messages to the user
/bin/echo Finished at: `date`
