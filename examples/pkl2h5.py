import numpy as np
import sys
import os
import filecmp
import subprocess
import cPickle as pickle
# import h5py
# import numpy as np
# from ys.mods import load
try:
    import h5features
except ImportError:
    sys.path.insert(0, os.path.join(
        os.path.dirname(os.path.dirname(os.path.dirname(
            os.path.realpath(__file__)))), 'h5features'))
    import h5features

with open(sys.argv[1], 'rb') as f:
    files2features=pickle.load(f)

total_size_pkl=0
for filename in files2features:
    total_size_pkl+=len(files2features[filename])
'''
for filename in files2features:
    for coli in range(len(files2features[filename])):
        norm2=0
        for rowi in range(len(files2features[filename][coli])):
            norm2+=files2features[filename][coli][rowi]*files2features[filename][coli][rowi]
        if norm2 ==0:
            num_replacements+=1
            for rowi in range(len(files2features[filename][coli])):
                files2features[filename][coli][rowi]=1.0
'''

files = list() # entry i is filename on line i
total_lines=0
files2times = dict()
first_line=True
for line in open(sys.argv[2], 'r'): # file list
    if not first_line:
        total_lines+=1
        tokens=line[:-1].split(' ')
        #name=tokens[0] + '_' + tokens[1].replace('.', '') + '_' + tokens[2].replace('.', '')
        name=tokens[0]
        files.append(name)
        if name not in files2times:
          files2times[name] = list()
        files2times[name].append((float(tokens[1]) + float(tokens[2]))/2)
    else:
        first_line=False

files = list(set(files)) # remove duplicates
features = []
times = []
for filename in files:
  features.append(np.array(files2features[filename]))
  times.append(np.array(files2times[filename]))

if os.path.exists('data.features'):
    os.remove('data.features')

print('files')
print(len(files))
print(files[0])
print('features')
print(len(features))
print(features[0])
print('times')
print(len(times))
print(times[0])
print('THESE 2 NEED TO BE EQUAL: ' + str(total_size_pkl) + ' AND ' + str(total_lines) +'; AND THESE OTHER 2 AS WELL: ' + str(len(files2times)) + ' AND ' + str(len(files2features)))

h5features.write('data.features', 'features', files, times, features)

"""
def generate_features(n_files, n_feat=2, max_frames=3, name='data.features'):
    if os.path.exists(name):
        os.remove(name)
    features = []
    times = []
    files = []
    for i in xrange(n_files):
        n_frames = np.random.randint(max_frames) + 1
        features.append(np.random.randn(n_frames, n_feat))
        times.append(np.linspace(0, 1, n_frames))
        files.append('s%d' % i)
    h5features.write(name, 'features', files, times, features)
"""
